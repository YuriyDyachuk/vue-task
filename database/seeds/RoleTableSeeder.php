<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        \DB::table('roles')->truncate();

        DB::table('roles')->insert([
            0 =>
                [
                    'id' => 1,
                    'name' => 'admin',
                    'type' => '0',
                    'parent_id' => '0',
                    'description' => '',
                ],
            1 =>
                [
                    'id' => 2,
                    'name' => 'client',
                    'type' => '0',
                    'parent_id' => '0',
                    'description' => '',
                ],
            2 =>
                [
                    'id' => 3,
                    'name' => 'logist',
                    'type' => '0',
                    'parent_id' => '1',
                    'description' => '',
                ],
            3 =>
                [
                    'id' => 4,
                    'name' => 'manager',
                    'type' => '0',
                    'parent_id' => '1',
                    'description' => '',
                ],
            4 =>
                [
                    'id' => 5,
                    'name' => 'cargo_loader',
                    'type' => '0',
                    'parent_id' => '1',
                    'description' => 'кладовщик',
                ],
            5 =>
                [
                    'id' => 6,
                    'name' => 'cargo_receiver',
                    'type' => '0',
                    'parent_id' => '1',
                    'description' => 'приемщик',
                ]
        ]);
    }
}
